<?php
$myBooleanVar=null; // returned by "True, False, 0, number, string, empty, null

if($myBooleanVar){
    echo '$myBooleanVar is True'.'<br>';
}
else{
    echo '$myBooleanVar is False'.'<br>';
}

//String example start

$myVar= 456;

$myStr="Hello World! $myVar"." <br>";
$myStr1='Hello World! $myVar'.' <br>';

echo $myStr;
echo $myStr1;

$heredocVar=<<<BITM
Hello World!  kjhkhdkfjhksfkj
jhfkjhskjsf $myStr
jhkshfoiurkweq,n
BITM;

echo $heredocVar.'<br>';

$nowdocVar=<<<'BITM'
Hello World!  kjhkhdkfjhksfkj
jhfkjhskjsf $myStr
jhkshfoiurkweq,n
BITM;

echo $nowdocVar.'<br><hr>';


// Indexed Array example

$myArray=array(13,true,75.25,"hello",5);

echo "<pre>";
print_r($myArray);
echo "</pre>"."<hr>";

echo "<pre>";
var_dump($myArray);
echo "</pre>"."<hr>";

// Associative Array Example
$personAge=array("10"=>50,"Rahim"=>35, "Karim"=>45, "Jarina"=>18, 60.35 );
echo $personAge["Rahim"]."<hr>";

echo "<pre>";
var_dump($personAge);
echo "</pre>"."<hr>";

$key=array_keys($personAge);
$value=array_values($personAge);

echo $key[1]."=>".$value[1];