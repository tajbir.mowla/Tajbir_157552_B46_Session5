<?php
$myVar= "132465abcd";
$myFloatValue= floatval($myVar);
echo $myFloatValue.'<br>';

$myVar="0.000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001";

if(empty($myVar)){
    echo '$myVar is empty'.'<br>';
}else{
    echo '$myVar is not empty'.'<br>';
}
echo "<hr>";

$myArr=array(12,"Hello", 4.3, true);

$mySerializedValue=serialize($myArr);

echo $mySerializedValue.'<hr>';

$unknownData= unserialize($mySerializedValue);

var_dump($unknownData);
echo '<hr>';
var_export($unknownData);

echo '<hr>';

//boolval("345");